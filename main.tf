resource "aws_dynamodb_table" "default" {
  name         = var.table_name
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = var.hash_key_column.name

  attribute {
    name = var.hash_key_column.name
    type = var.hash_key_column.type
  }

  dynamic "attribute" {
    for_each = var.indexed_columns
    content {
      name = attribute.key
      type = attribute.value
    }
  }

  dynamic "global_secondary_index" {
    for_each = var.indexed_columns
    content {
      name               = "${global_secondary_index.key}Index"
      hash_key           = global_secondary_index.key
      projection_type    = "INCLUDE"
      non_key_attributes = [var.hash_key_column.name]
    }
  }

  tags = {
    env = var.environment
  }
}
