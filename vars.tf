variable "environment" {
  description = "environment where this resource is used"
  type        = string
}

variable "table_name" {
  description = "name of the DynamoDB table"
  type        = string
}

variable "hash_key_column" {
  description = "hash key column name and type of the DynamoDB table"
  type        = object({
    name = string,
    type = string,
  })
}

variable "indexed_columns" {
  description = "indexed columns of the DynamoDB table"
  type        = map(string)
  default     = {}
}